#!/usr/bin/env python
# -*- coding: utf-8 -*-

# Jogo Sudoku
# Objetivo desse programa em Python 2 é o estudo e testes da linguagem.
# Autor  : Edgar Bruno
# E-mail : edgar.brunos@gmail.com

import pygtk , pango, gtk, gobject, time
pygtk.require('2.0')
from random import randint

class Validator:
	def __init__(self, indexMatriz=None, makeListaSM=None, flag=None):

		self.posFix = [0, 3 , 6 , 27, 30, 33, 54, 57, 60]	# Posições iniciais de cada quadrante do jogo	
		self.indexMatriz = indexMatriz 						# index da matriz
		self.makeListaSM = makeListaSM
		self.flag= flag

	def abscissas(self):
		# Verifica se há ocorrência de números repetidos no eixo X

		i = self.indexMatriz
		dicAOQ = {}

		while i % 9 != 0: # Encontra o primeiro número index da linha
			i -= 1



		while not len(dicAOQ) == 9: # Monta o dicionario da abscissa
			dicAOQ[i]=self.makeListaSM[i]
			i +=1
		return dicAOQ

	def ordenadas(self):
		# Verifica se há ocorrência de números repetidos no eixo Y
		i = self.indexMatriz		

		while i >= 9:
			i -= 9
		if self.flag:
			dicAOQ = {}
			while not len(dicAOQ) == 9: # Monta o dicionario da ordenadas
				dicAOQ[i]=self.makeListaSM[i]
				i += 9				
		else:
			dicAOQ = self.makeListaSM[i::9]

		return dicAOQ

	def quadrante(self):
		# Verifica se há ocorrência de números repetidos no quadrante

		quadMontado = []
		y = 0
		inIdx = 0
		for i in self.posFix:
			z = 0
			validarQuad =[]
			for x in range(9):

				validarQuad.append(i+z)
				z += 1
				y += 1

				if y > 2:
					y = 0
					z += 6

			if self.indexMatriz in validarQuad:
				inIdx = i
				break
		z = 0
		
		if self.flag:
			dicAOQ = {}
			for i in validarQuad:
				dicAOQ[i] = self.makeListaSM[i]
			return dicAOQ
		else:			
			for i in range(3):
				quadMontado.extend(self.makeListaSM[inIdx+z:inIdx+3+z])
				z += 9
			checkRepeti = quadMontado
		return checkRepeti

	def verificador(self):

		dicR={}
		listV=[]
		for dic in [self.abscissas(), self.ordenadas(), self.quadrante()]:

			for i in dic:
				listOfKeys = [key  for (key, value) in dic.items() if value == dic[i]]
				if dic[i]:
					if not len(listOfKeys) > 1:
						for j in listOfKeys:
							dicR[j]=False
					else:
						dicR[i]=True
				else:
					dicR[i]=False

			listV.append(dicR[self.indexMatriz])
		if True in listV:
			dicR[self.indexMatriz] = True

		if not dicR[self.indexMatriz] and 0 not in self.makeListaSM:
			# Verifica se o jogo chegou a uma solução válida e finaliza o jogo
			print "YOU WIN !!! Game Over"
			message = gtk.MessageDialog(
										type 			 =  gtk.MESSAGE_INFO,
										message_format	 = "YOU WIN !!! Game Over",
									 	buttons 		 = gtk.BUTTONS_OK,
									 	)
			message.run()

		return dicR	

class Matriz_maker:
	
	def __init__(self, tentativas=0):
		# sys.setcursionlimit()
		self.listaSudokuMatriz = []
		self.numD = list(range(1, 10))		# Números disponíveis : == Abscissas
		self.listaT = [] 					# Lista temporária

	def sorteio_numerico(self):

		idx=len(self.listaSudokuMatriz) + len(self.listaT) # Index da matriz

		lp = Validator(idx, self.listaSudokuMatriz)

		numDT=list(set(self.numD) - set(lp.ordenadas())- set(lp.quadrante()))
			# Verifica a quais numeros são validos para posição da matriz

		if not numDT:
			self.listaT = []	
			self.numD = list(range(1, 10))
			return self.sorteio_numerico()

		vAux = numDT[randint(0, len(numDT)-1)]

		self.listaT.append(vAux)
		self.numD.remove(vAux) 

		if len(self.listaT) == 9:
			self.listaSudokuMatriz.extend(self.listaT)
			return self.listaSudokuMatriz
		else:
			return self.sorteio_numerico()
		return self.listaT

class Ocultador:

	def __init__(self, listaSudokuMatriz, hidden):
		self.listaSudokuMatrizOculta = list(listaSudokuMatriz)
		self.listaSudokuMatriz = listaSudokuMatriz
		print listaSudokuMatriz

		self.choice = { # 
				1 : self.hidden_A(),
				2 : self.hidden_B(),

		}.get(hidden)
	
	def hidden_A(self):
		
		listaOcultados = [] # Evitar repetição de indeces
		while not len(listaOcultados) == randint(40, 60):
		# Quantidade limite de números ocultados
			ocultar = randint(0,80) # 

			if not ocultar in listaOcultados:
				listaOcultados.append(ocultar)
				self.listaSudokuMatrizOculta[ocultar] = 0
				
		return self.finisher(map(list, zip(self.listaSudokuMatrizOculta)))
		# Transforma a lista em uma lista de lista.

	def hidden_B(self):
		return "HIDDEN_B"

	def finisher(self, mll):

		dicValores = {}

		for i, v in enumerate(mll):

			v=True if not mll[i][0] else False

			mll[i].append(v)
			dicValores= dict(zip(range(len(mll)), mll))
			dicValores["listaSudokuMatriz"] = self.listaSudokuMatriz
			dicValores["listaSudokuMatrizOculta"] = self.listaSudokuMatrizOculta

		return dicValores

class MakeWidgets:

		def __init__(self, dicValores):
			self.dicValores = dicValores
	
		def mainWidgets(self):

			vbox = gtk.VBox(False, 5)
			table = gtk.Table(3,3, gtk.TRUE)

			x, y = 0, 0

			for quadNumero, i in enumerate(Validator().posFix):

				eventBox0 = gtk.EventBox()
				eventBox0.connect('show', self.colorificador, None, 2)
				
				table_b = gtk.Table(3,3, gtk.TRUE)
				frame = gtk.Frame()
		
				quadMontado, xx, yy, z = [], 0, 0, 0

				for iq in range(3):

					for d in range(i+z, i+3+z):
						#print "Indice = %d Valor = %d" % (d, self.matrizO[d])
						# A key do dicionário é o index da matrizO. O Seu valor será o elemento label
						quadMontado.append(d)

					z += 9
					
				for idNum in quadMontado:

					label = gtk.Label()
					attr = pango.AttrList()
					eventBox = gtk.EventBox()
					eventBox.connect('focus', self.colorificador, None, quadNumero, idNum)

					if self.dicValores[idNum][1]:

						label.set_text(str(""))
						size = pango.AttrSize(22500, 0, 2)
						eventBox.connect("button-press-event", 	 self.clickJogo, label, idNum)
						eventBox.connect("button-release-event", self.colorificador, 4)
						eventBox.connect("button-press-event",   self.colorificador, 8)
						eventBox.connect("enter-notify-event",   self.colorificador, 3) # Item Selecionado
						eventBox.connect("leave-notify-event",   self.colorificador, None, quadNumero, idNum ) # Retorna cor Padrão

					else:

						# Cor da fonte não clicavel
						label.set_markup("<b>%s</b>" % str(self.dicValores[idNum][0]))
						label.connect('show', self.colorificador, None, 5)
						size = pango.AttrSize(25000, 0, 1)
						eventBox.set_border_width(2)
						eventBox.set_tooltip_text("Não clicavel")
						
					attr.insert(size)		
					label.set_attributes(attr)
					label.set_size_request(40, 40)
					self.dicValores[idNum].append(label)			
					eventBox.add(label)
					table_b.attach(eventBox, xx, xx+1, yy, yy+1, xpadding=1, ypadding=1)

					xx += 1
					if xx > 2:
						xx = 0
						yy += 1

				frame.add(table_b)
				eventBox0.add(frame)

				table.attach(eventBox0, x, x+1, y, y+1, xpadding=1, ypadding=1)

				x += 1
				if x > 2:
					x = 0
					y += 1

			framez = gtk.Frame()
			framez.add(table)

			vbox.pack_start(framez, False, False, 3)
				
			return vbox

		def clickJogo(self, widget, event, label, indexMatriz):

			valueC = 0 if label.get_text() == "" else int(label.get_text())

			if event.button == 1:
				valueC += 1
				valueC = 1 if valueC > 9 else valueC

			elif event.button == 2:
					valueC = 0
			else:
				valueC -= 1
				valueC = 9 if valueC < 1 else valueC 
				
			valueCL = "" if valueC == 0 else str(valueC)	
			label.set_text(valueCL)

			self.dicValores["listaSudokuMatrizOculta"][indexMatriz] = valueC
			self.dicValores[indexMatriz][0] = valueC

			colorV = Validator(indexMatriz, self.dicValores["listaSudokuMatrizOculta"], True).verificador()
			
			for i in colorV.keys():
				color = 1 if colorV[i] else 2
				# color = [1 if colorV[i] else 2 for i in colorV.keys()]
				self.colorificador(self.dicValores[i][2], None, color)
				
		def colorificador(self, widget, event, cor=None ,quadNumero=None, idNum=None):

			#controla todas cores do programa.
			if not cor:
				if quadNumero % 2 == 0:
					cor = 10 if idNum % 2 == 0 else 11
					# Cor 1 do fundo clicavel
				else:
					cor = 12 if idNum % 2 == 0 else 13
					# Cor 2 do fundo clicavel

			cc ={
					1	:	"#D40000", # Cor Repedito
					2	:	"#282828", # Cor do Fundo do Quadrande BASE
					3	:	"#35683D", # Item Selecionado
					4	:	"#008000", # Item Clicado	
					5	:	"#1F2021", # Cor padrão
					8	:	"#009600",
					10	:	"#676767",
					11	:	"#797979",
					12	:	"#595959",
					13	:	"#4A4A4A",
				}.get(cor,  "purple") # Cor Não Clicavel

			if str(type(widget)) == "<type 'gtk.Label'>":
				return widget.modify_fg(gtk.STATE_NORMAL, gtk.gdk.Color(cc))
			else:
				return widget.modify_bg(gtk.STATE_NORMAL, gtk.gdk.Color(cc))

class ProgressBar:

	def __init__(self):
		self.pBar = gtk.ProgressBar()
		self.pBar.set_fraction(0.0)
		self.pBar.set_text(" ")

	def mainWidgetsPB(self):

		self.buttoS= gtk.Button()

		self.buttoN = gtk.Button("Create")
		self.buttoN.set_tooltip_text("Create a New game")
		self.buttoN.connect("clicked", self.btn_clickPB)

		self.buttoL = gtk.Button("Load")
		self.buttoL.set_tooltip_text("Load a salved game")

		hBox = gtk.HBox(homogeneous=True, spacing=0)
		hBox.pack_start(self.buttoN)
		hBox.pack_start(self.buttoL)
		hBox.pack_start(self.buttoS)

		align = gtk.Alignment(0.5, 0.9 ,0.25, 0)
		alignb = gtk.Alignment(0.5, 0 ,0.25, 0)
		align.add(hBox)

		vBox = gtk.VBox()
		vBox.add(align)
		alignb.add(self.pBar)
		vBox.add(alignb)

		return vBox

	def btn_clickPB(self, widget):
		self.buttoL.hide()
		self.buttoN.hide()
		self.buttoS.set_sensitive(False)
		self.buttoS.set_tooltip_text("Wait... ")
		self.pBar.set_fraction(0)
		self.timer = gobject.timeout_add (100, self.progressBar, Matriz_maker())

	def progressBar(self, obj):

		nv = obj.sorteio_numerico()

		if len(nv) == 81:
			self.buttoS.set_sensitive(True)
			self.buttoS.set_label("Start")
			self.buttoS.connect("clicked", self.open, nv)

		else:
			nv = len(nv) * 0.0138

			self.pBar.set_fraction(nv)
			self.pBar.set_text(str(nv*100))
			return True

	def open(self, widget, lstaSo):
		x=WindowG(Ocultador(lstaSo, 1).choice)
		x.main()

class WindowG:

	def __init__(self, winStar=None):

		self.window = gtk.Window(gtk.WINDOW_TOPLEVEL)
		self.window.connect('destroy', lambda w: gtk.main_quit())
		self.window.set_title("Sudoku")
		self.window.set_position(gtk.WIN_POS_CENTER)
		self.window.set_size_request(430, 435)
		self.window.set_resizable(False)
		vbox = gtk.VBox(homogeneous=True, spacing=0)
		
		if not winStar:
			vbox.pack_start(ProgressBar().mainWidgetsPB())
		else:
			vbox.pack_start(MakeWidgets(winStar).mainWidgets())
		#vbox.pack_start(MakeWidgets(Ocultador(Matriz_maker().sorteio_numerico(), 1).choice).mainWidgets())
		# Aqui  será inseirdo uma condição para restaurar um jogo salvo
		# Dicionário conterá todos os valores para identificar o botão, valor e indice.

		self.window.add(vbox)	
		self.window.show_all()

	def main(self):
		gtk.main()
			
if __name__ == "__main__":
	x=WindowG()
	x.main()