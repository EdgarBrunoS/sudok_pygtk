# -*- coding: utf-8 -*-

# Jogo Sudoku
# Objetivo desse programa em Python 3 é o estudo/testes da linguagem.
# Autor  : Edgar Bruno
# e-mail : edgar.brunos@gmail.com
#

import gi
gi.require_version('Gtk', '3.0')
from gi.repository import Gtk

class DialobB(Gtk.Dialog):

	def __init__(self, parent):
		Gtk.Dialog.__init__(self, "My Dialog",
							parent,
							10,
							(Gtk.STOCK_CANCEL,
							Gtk.ResponseType.CANCEL,
							Gtk.STOCK_OK,
							Gtk.ResponseType.OK))
	def on_response(self, widget, response_id):
		self.result = self.entry.get_text ()
	def get_result(self):
		return self.result


class GameWindow(Gtk.Window):
	def __init__(self):
		Gtk.Window.__init__(self, title="Tela GTK3")
		self.set_border_width(4)
		self.set_size_request(430, 435)
		self.set_resizable(False)
		aboutBTN = Gtk.ToolButton(Gtk.STOCK_ABOUT)
		aboutBTN.connect("clicked", self.on_info_clicked)

		toolbar = Gtk.Toolbar()
		toolbar.insert(aboutBTN, -1)

		button = Gtk.Button(label="Open dialog")
		button.connect("clicked", self.on_button_clicked)


		box0 = Gtk.Box(orientation=Gtk.Orientation.VERTICAL, spacing=6)
		box0.pack_start(toolbar, False, False, 0)
		box0.add(button)
		self.add(box0)

	def on_button_clicked(self, widget):
		dialog = DialobB(self)
		response = dialog.run()
        
		if response == Gtk.ResponseType.OK:
			#self.label.set_text(dialog.get_result())
			print("The OK button was clicked")
		elif response == Gtk.ResponseType.CANCEL:
			print("The Cancel button was clicked")
		dialog.destroy()


	def on_info_clicked(self, widget):
		dialog = Gtk.MessageDialog(
								   self,
						   			  0,
				   Gtk.MessageType.INFO,
			         Gtk.ButtonsType.OK,
		"This is an INFO MessageDialog",)

		dialog.format_secondary_text("And this is the secondary text that explains things.")
		dialog.run()
		print("INFO dialog closed")
		dialog.destroy()

		
if __name__ == "__main__":
	win = GameWindow()
	win.connect('destroy', Gtk.main_quit)
	win.show_all()
	Gtk.main()